﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FamilyManager.Core.Entities;
using FamilyManager.Core.Repositories;
using FamilyManager.Core.Services;

namespace FamilyManager.Infrustructure.Services
{
    public class ChildService : IChildService
    {
        private readonly IChildRepository _childRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ChildService(IUnitOfWork unitOfWork, IChildRepository childRepository)
        {
            _childRepository = childRepository;
            _unitOfWork = unitOfWork;
        }

        public void Save(Child entity)
        {
            entity.ApprovalStatusId = (byte?)ApprovalStatusType.Pending;
            _childRepository.Save(entity);
            _unitOfWork.Commit();
        }

        public Child Get(int id)
        {
            var entity = _childRepository.Get(id);
            return entity;
        }

        public void Approve(int id)
        {
            throw new NotImplementedException();
        }

        public void AssignParents(int id, int firstParentId, int secondParentId)
        {
            if (firstParentId == secondParentId)
            {
                return;
            }
            var child = _childRepository.Get(id);
            if (firstParentId > 0 && child.FirstParentId != firstParentId)
            {
                child.FirstParentId = firstParentId;
            }
            if (secondParentId > 0 && child.SecondParentId != secondParentId)
            {
                child.SecondParentId = secondParentId;
            }
            _unitOfWork.Commit();
        }

        public IEnumerable<Child> FindAllUnApproved()
        {
            var query = _childRepository.FindAll( x=>x.ApprovalStatusId ==null || x.ApprovalStatusId ==(byte)ApprovalStatusType.Pending);
            return query.ToList();
        }
    }
}
