﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FamilyManager.Core.Entities;
using FamilyManager.Core.Repositories;
using FamilyManager.Core.Services;

namespace FamilyManager.Infrustructure.Services
{
    public class ParentService : IParentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IParentRepository _parentRepository;
        private readonly IChildRepository _childRepository;

        public ParentService( IUnitOfWork unitOfWork,IParentRepository parentRepository,IChildRepository childRepository)
        {
            _unitOfWork = unitOfWork;
            _parentRepository = parentRepository;
            _childRepository = childRepository;
        }

        public void Save(Parent entity)
        {
            _parentRepository.Save(entity);
            _unitOfWork.Commit();
        }

        public Parent Get(int id)
        {
            return _parentRepository.Get(id);
        }

        public IEnumerable<Parent> FindAll()
        {
            return _parentRepository.FindAll().ToList();
        }

        public void AddChild(int parentId, int childId)
        {
            var child = _childRepository.Get(childId);
            if (child.FirstParentId == null)
                child.FirstParentId = parentId;
            _unitOfWork.Commit();
        }
    }
}
