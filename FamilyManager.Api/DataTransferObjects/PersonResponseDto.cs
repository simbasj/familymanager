﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FamilyManager.Api.DataTransferObjects
{
    public abstract class PersonResponseDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int GenderId { get; set; }
        public string GenderName { get; set; }
    }
}