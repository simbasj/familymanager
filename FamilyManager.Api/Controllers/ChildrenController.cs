﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FamilyManager.Api.DataTransferObjects;
using FamilyManager.Core.Entities;
using FamilyManager.Core.Services;

namespace FamilyManager.Api.Controllers
{
    /// <summary>
    /// API (Version 1) for interacting with parents
    /// </summary>
    [RoutePrefix("api/v1")]
    public class ChildrenController : BaseApiController
    {
        private readonly IChildService _childService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="childService"></param>
        public ChildrenController(IChildService childService)
        {
            _childService = childService;
        }

        /// <summary>
        /// Get all unapproved children
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("children")]
        [ResponseType(typeof(IEnumerable<ChildResponseDto>))]
        public IHttpActionResult GetChildren()
        {
            try
            {
                var children = _childService.FindAllUnApproved();
                IEnumerable<ChildResponseDto> childrenResponse = new List<ChildResponseDto>();
                if (children.Any())
                {
                    childrenResponse = Map<IEnumerable<ChildResponseDto>, IEnumerable<Child>>(children);
                }
                return Ok(childrenResponse);
            }
            catch (Exception exception)
            {
                var response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
                throw new HttpResponseException(response);
            }
        }

        /// <summary>
        /// Get child details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("children/{id}", Name = "GetChild")]
        [ResponseType(typeof(ChildResponseDto))]
        public IHttpActionResult GetChild(int id)
        {
            try
            {
                var child = _childService.Get(id);
                if (child == null)
                    return NotFound();

                var childResponse = Map<ChildResponseDto, Child>(child);
                return Ok(childResponse);
            }
            catch (Exception exception)
            {
                var response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
                throw new HttpResponseException(response);
            }
        }

        /// <summary>
        /// Add a child
        /// </summary>
        /// <param name="childRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("children")]
        [ResponseType(typeof(ChildResponseDto))]
        public IHttpActionResult AddParent(ChildRequestDto childRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var child = Map<Child, ChildRequestDto>(childRequest);
                _childService.Save(child);
                var childResponse = Map<ChildResponseDto, Child>(child);
                return CreatedAtRoute("GetChild", new { id = childResponse.Id }, childResponse);
            }
            catch (Exception exception)
            {
                var response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
                throw new HttpResponseException(response);
            }
        }

        /// <summary>
        /// Assign parents to a child
        /// </summary>
        /// <param name="id">Id of child to be updated </param>
        /// <param name="firstParentId">First parent id</param>
        /// <param name="secondParentId">Second parent id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("children/{id}/{firstParentId}/{secondparentId}")]
        public IHttpActionResult AssignParents(int id, int firstParentId, int secondParentId)
        {
            try
            {
                _childService.AssignParents(id, firstParentId, secondParentId);
                return Ok();
            }
            catch (Exception exception)
            {
                var response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
                throw new HttpResponseException(response);
            }
        }


    }
}
