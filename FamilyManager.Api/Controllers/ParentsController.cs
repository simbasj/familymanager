﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FamilyManager.Api.DataTransferObjects;
using FamilyManager.Core.Entities;
using FamilyManager.Core.Services;

namespace FamilyManager.Api.Controllers
{
    /// <summary>
    /// API (Version 1) for interacting with parents
    /// </summary>
    [RoutePrefix("api/v1")]
    public class ParentsController : BaseApiController
    {
        private readonly IParentService _parentService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentService"></param>
        public ParentsController(IParentService parentService)
        {
            _parentService = parentService;
        }

        /// <summary>
        /// Get all parents
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("parents")]
        [ResponseType(typeof(IEnumerable<ParentResponseDto>))]
        public IHttpActionResult GetParents()
        {
            try
            {                
                var parents = _parentService.FindAll();
                IEnumerable<ParentResponseDto> parentsResponse = new List<ParentResponseDto>();
                if (parents.Any())
                {
                    parentsResponse = Map<IEnumerable<ParentResponseDto>, IEnumerable<Parent>>(parents);                    
                }
                return Ok(parentsResponse);
            }
            catch (Exception exception)
            {
                var response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
                throw new HttpResponseException(response);
            }
        }
        /// <summary>
        /// Get parent details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("parents/{id}", Name = "GetParent")]
        [ResponseType(typeof(ParentResponseDto))]
        public IHttpActionResult GetParent(int id)
        {
            try
            {
                var parent = _parentService.Get(id);
                if (parent == null)
                    return NotFound();

                var parentResponse = Map<ParentResponseDto, Parent>(parent);
                return Ok(parentResponse);
            }
            catch (Exception exception)
            {
                var response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
                throw new HttpResponseException(response);
            }
        }

        /// <summary>
        /// Add a parent
        /// </summary>
        /// <param name="parentRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("parents")]
        [ResponseType(typeof(ParentResponseDto))]
        public IHttpActionResult AddParent(ParentRequestDto parentRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var parent = Map<Parent, ParentRequestDto>(parentRequest);
                _parentService.Save(parent);
                var parentResponse = Map<ParentResponseDto, Parent>(parent);
                return CreatedAtRoute("GetParent", new { id = parentResponse.Id }, parentResponse); ;
            }
            catch (Exception exception)
            {
                var response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
                throw new HttpResponseException(response);
            }
        }

    }
}
