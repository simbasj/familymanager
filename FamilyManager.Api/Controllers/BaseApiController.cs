﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FamilyManager.Api.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        protected TDestination Map<TDestination, TSource>(TSource source)
        {
            return AutoMapperConfig.Map<TDestination,TSource>(source);
        }
    }
}
