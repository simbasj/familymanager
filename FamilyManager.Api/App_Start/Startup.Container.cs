﻿using System.Web.Http;
using LightInject;

namespace FamilyManager.Api
{
    public partial class Startup
    {
        public void ConfigureContainer()
        {
            var container = new ServiceContainer();
            container.RegisterAssembly("FamilyManager.DependencyResolver.dll");
            container.EnableWebApi(GlobalConfiguration.Configuration);
            container.EnableMvc();
            container.RegisterControllers();
            container.RegisterApiControllers();
            container.EnablePerWebRequestScope();

        }
    }
}