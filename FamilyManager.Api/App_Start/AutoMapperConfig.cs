﻿using AutoMapper;
using FamilyManager.Api.DataTransferObjects;
using FamilyManager.Core.Entities;

namespace FamilyManager.Api
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<ParentRequestDto, Parent>();
            Mapper.CreateMap<Parent, ParentResponseDto>()
                  .ForMember(dest => dest.GenderName, opt => opt.MapFrom(src => src.Gender.DisplayName));
            Mapper.CreateMap<ChildRequestDto, Child>();
            Mapper.CreateMap<Child, ChildResponseDto>()
                  .ForMember(dest => dest.GenderName, opt => opt.MapFrom(src => src.Gender.DisplayName));
            //Mapper.AssertConfigurationIsValid();
        }

        public static TDestination Map<TDestination, TSource>(TSource source)
        {
            var result = Mapper.Map<TDestination>(source);
            return result;
        }
    }
}