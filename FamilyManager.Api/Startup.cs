﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(FamilyManager.Api.Startup))]

namespace FamilyManager.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureContainer();
        }
    }
}
