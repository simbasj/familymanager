﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FamilyManager.Core.Entities;
using FamilyManager.Core.Repositories;
using FamilyManager.Core.Services;
using FamilyManager.Data.SqlServer;
using FamilyManager.Data.SqlServer.Repositories;
using FamilyManager.Infrustructure.Services;
using LightInject;

namespace FamilyManager.DependencyResolver
{
    public class CompositionRoot : ICompositionRoot
    {
        public void Compose(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.Register<DatabaseContext>(new PerScopeLifetime());
            serviceRegistry.Register<IUnitOfWork, UnitOfWork>(new PerScopeLifetime());
            serviceRegistry.Register<IChildRepository, ChildRepository>(new PerScopeLifetime());
            serviceRegistry.Register<IParentRepository, ParentRepository>(new PerScopeLifetime());
            serviceRegistry.Register<IChildService, ChildService>(new PerScopeLifetime());
            serviceRegistry.Register<IParentService, ParentService>(new PerScopeLifetime());
        }
    }
}
