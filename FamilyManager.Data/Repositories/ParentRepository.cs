﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FamilyManager.Core.Entities;
using FamilyManager.Core.Repositories;

namespace FamilyManager.Data.SqlServer.Repositories
{
    public class ParentRepository : IParentRepository
    {
        private readonly DatabaseContext _databaseContext;

        public ParentRepository(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public void Save(Parent entity)
        {          
            _databaseContext.Person.Add(entity);
        }

        public Parent Get(int id)
        {
            var entity = _databaseContext.Person.OfType<Parent>()
                         .FirstOrDefault(x => x.Id == id);
            return entity;
        }

        public IQueryable<Parent> FindAll()
        {
            var entities = _databaseContext.Person.OfType<Parent>();
            return entities;
        }
    }
}
