﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using FamilyManager.Core.Entities;
using FamilyManager.Core.Repositories;

namespace FamilyManager.Data.SqlServer.Repositories
{
    public class ChildRepository :  IChildRepository
    {
        private readonly DatabaseContext _databaseContext;

        public ChildRepository(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public void Save(Child entity)
        {
            _databaseContext.Person.Add(entity);
        }

        public Child Get(int id)
        {
            var entity = _databaseContext.Person.OfType<Child>()
                         .FirstOrDefault(x => x.Id == id);
            return entity;
        }

        public IQueryable<Child> FindAll()
        {
            var entities = _databaseContext.Person.OfType<Child>();
            return entities;
        }

        public IQueryable<Child> FindAll(Expression<Func<Child, bool>> predicate)
        {
            var entities = _databaseContext.Person.OfType<Child>().Where(predicate);
            return entities;
        }
    }
}
