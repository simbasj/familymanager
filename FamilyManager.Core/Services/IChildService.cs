﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FamilyManager.Core.Entities;

namespace FamilyManager.Core.Services
{
    public interface IChildService : IPersonService<Child>
    {
        void Approve(int id);
        void AssignParents(int id, int firstParentId, int secondParentId);
        IEnumerable<Child> FindAllUnApproved();
    }
}
