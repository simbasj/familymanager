﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FamilyManager.Core.Entities;

namespace FamilyManager.Core.Services
{
    public interface IParentService:IPersonService<Parent>
    {
        IEnumerable<Parent> FindAll();
    }
}
