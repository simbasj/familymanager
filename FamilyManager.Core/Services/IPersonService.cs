﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FamilyManager.Core.Entities;

namespace FamilyManager.Core.Services
{
    public interface IPersonService<T>
    {
        void Save(T entity);
        T Get(int id);         
    }
}
