﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FamilyManager.Core.Entities;

namespace FamilyManager.Core.Repositories
{
    public interface IChildRepository:IPersonRepository<Child>
    {
        IQueryable<Child> FindAll(Expression<Func<Child, bool>> predicate);
    }
}
