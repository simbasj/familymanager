﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FamilyManager.Core.Entities;

namespace FamilyManager.Core.Repositories
{
    public interface IPersonRepository<T> 
    {
        void Save(T entity);
        T Get(int id);
        IQueryable<T> FindAll();
    }
}
