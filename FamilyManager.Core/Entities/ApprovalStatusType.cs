﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyManager.Core.Entities
{
    public enum ApprovalStatusType:byte
    {
        Pending=1,
        Declined = 2,
        Approved = 3,
    }
}
