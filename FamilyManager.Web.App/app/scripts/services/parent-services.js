'use strict';

var services = angular.module('familyManager.parentServices', []);

services.factory('Parent', function ($resource) {
	return $resource('http://localhost:53080/api/v1/parents/:id', {
		id: '@id'
	}, {
		update: {
			method: 'PUT'
		}
	});
});