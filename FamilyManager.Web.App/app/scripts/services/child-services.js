'use strict';

var services = angular.module('familyManager.childServices', []);

services.factory('Child', function ($resource) {

	var baseUrl = 'http://localhost:53080/api/v1/children/:id';
	return $resource(baseUrl, {
		id: '@id'
	}, {
		update: {
			method: 'PUT'
		},
		assignParents: {
			method: 'POST',
			url: baseUrl + '/:firstParentId/:secondParentId',
			params: {
				id: '@id',
				firstParentId: '@firstParentId',
				secondParentId: '@secondParentId'
			}
		}
	});
});