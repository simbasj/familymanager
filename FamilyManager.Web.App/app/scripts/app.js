'use strict';

angular.module('familyManager', ['ui.router', 'ngResource', 'familyManager.parentControllers', 'familyManager.parentServices', 'familyManager.childControllers', 'familyManager.childServices']);

angular.module('familyManager')
	.config(['$httpProvider', function ($httpProvider) {

			$httpProvider.defaults.useXDomain = true;
			delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]).config(function ($stateProvider) {
		$stateProvider.state('parents', { // state for showing all movies
			url: '/parents',
			templateUrl: 'views/parents.html',
			controller: 'ParentListController'
		}).state('newParent', { //state for adding a new parent
			url: '/parents/new',
			templateUrl: 'views/parent-add.html',
			controller: 'ParentCreateController'
		}).state('unapprovedChildren', { // state for showing all customers
			url: '/children/unapproved',
			templateUrl: 'views/children.html',
			controller: 'ChildrenListController'
		}).state('newChild', { //state for adding a new customer
			url: '/children/new',
			templateUrl: 'views/child-add.html',
			controller: 'ChildCreateController'
		}).state('assignChildParents', { // state for showing all customers
			url: '/children/:id/assignParents',
			templateUrl: 'views/child-assign-parents.html',
			controller: 'ChildEditController'
		});
	}).run(function ($state) {
		$state.go('parents'); //make a transition to movies state when app starts
	});