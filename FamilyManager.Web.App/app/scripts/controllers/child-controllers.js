'use strict';

angular.module('familyManager.childControllers', [])
	.controller('ChildrenListController', function ($scope, $state, $window, Child) {
		$scope.people = Child.query(); //fetch all children. Issues a GET to /api/v1/children

	}).controller('ChildViewController', function ($scope, $stateParams, Child) {
		$scope.person = Child.get({
			id: $stateParams.id
		}); //Get a single child.Issues a GET to /api/children/:id
	}).controller('ChildCreateController', function ($scope, $state, $stateParams, Child) {
		$scope.person = new Child(); //create new child instance. Properties will be set via ng-model on UI

		$scope.genders = [{
			name: 'Male',
			id: 1
		}, {
			name: 'Female',
			id: 2
		}];

		$scope.addChild = function () { //create a new child. Issues a POST to /api/v1/children
			$scope.person.$save(function () {
				$state.go('unapprovedChildren'); // on success go back to home i.e. children state.
			});
		};

	}).controller('ChildEditController', function ($scope, $state, $stateParams, Child, Parent) {

		$scope.parents = Parent.query(); //fetch all parents. Issues a GET to /api/v1/parents

		$scope.assignParents = function () {
			Child.assignParents({
				id: $stateParams.id,
				firstParentId: $scope.person.firstParentId,
				secondParentId: $scope.person.secondParentId
			}, function () {
				$state.go('unapprovedChildren'); // on success go back to home i.e. unapprovedChildren state.
			});
		};

		$scope.loadChild = function () { //Issues a GET request to /api/v1/children:id to get a child to update

			$scope.person = Child.get({
				id: $stateParams.id
			});
		};

		$scope.loadChild(); // Load a child which can be edited on UI

	});