'use strict';

angular.module('familyManager.parentControllers', [])
	.controller('ParentListController', function ($scope, $state, $window, Parent) {
		$scope.people = Parent.query(); //fetch all parents. Issues a GET to /api/v1/parents

	}).controller('ParentViewController', function ($scope, $stateParams, Parent) {
		$scope.person = Parent.get({
			id: $stateParams.id
		}); //Get a single parent.Issues a GET to /api/parents/:id
	}).controller('ParentCreateController', function ($scope, $state, $stateParams, Parent) {
		$scope.person = new Parent(); //create new parent instance. Properties will be set via ng-model on UI

		$scope.genders = [{
			name: 'Male',
			id: 1
		}, {
			name: 'Female',
			id: 2
		}];

		$scope.addParent = function () { //create a new parent. Issues a POST to /api/v1/parents
			$scope.person.$save(function () {
				$state.go('parents'); // on success go back to home i.e. parents state.
			});
		};
	}).controller('ParentEditController', function ($scope, $state, $stateParams, Parent) {
		$scope.updateParent = function () { //Update the edited parent. Issues a PUT to /api/v1/parents/:id
			$scope.person.$update(function () {
				$state.go('parents'); // on success go back to home i.e. parents state.
			});
		};

		$scope.loadParent = function () { //Issues a GET request to /api/v1/parents:id to get a parent to update
			$scope.person = Parent.get({
				id: $stateParams.id
			});
		};

		$scope.loadParent(); // Load a parent which can be edited on UI
	});